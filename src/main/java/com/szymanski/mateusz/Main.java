package com.szymanski.mateusz;

import org.neo4j.driver.v1.*;
import static  org.neo4j.driver.v1.Values.parameters;


public class Main {

    public static void main(String[] args) {
	// write your code here

        Driver driver = GraphDatabase.driver("bolt://127.0.0.1", AuthTokens.basic("sejman","sejman"));

        Session session = driver.session();
        session.run(
                "CREATE (a:GeoCoord {long: {long},lat: {lat}})",parameters("long", "54.00","lat","23.33"));

        StatementResult result = session.run( "MATCH (a:GeoCoord) WHERE a.long = {long} " +
                        "RETURN a.long AS long, a.lat AS lat",
                parameters( "long", "54.00" ) );

        while ( result.hasNext()) {
            Record record = result.next();
            System.out.println(record.get("long").asString()+" " + record.get("lat").asString());
        }

        session.close();
        driver.close();
    }
}
